<?php

/**
 * @file
 * Administrative page callbacks for the block_order module.
 */

/**
 * Implements hook_admin_settings() for module settings configuration.
 */
function block_order_admin_settings_form($form_state) {
  // Region settings.
  $form['regions'] = array(
    '#type' => 'fieldset',
    '#title' => t('Region settings'),
    '#collapsible' => FALSE,
    '#description' => t('Specify the regions for each theme that will have the block order control enabled.'),
    '#tree' => TRUE,
  );

  $theme_default = variable_get('theme_default', 'bartik');
  $admin_theme = variable_get('admin_theme');
  foreach (list_themes() as $key => $theme) {
    // Only display enabled themes
    if ($theme->status) {
      // Use a meaningful title for the main site theme and administrative
      // theme.
      $theme_title = $theme->info['name'];
      if ($key == $theme_default) {
        $theme_title = t('!theme (default theme)', array('!theme' => $theme_title));
      }
      elseif ($admin_theme && $key == $admin_theme) {
        $theme_title = t('!theme (administration theme)', array('!theme' => $theme_title));
      }
      $settings = variable_get('block_order_' . $key . '_regions', array());
      $form['regions'][$key] = array(
        '#type' => 'select',
        '#title' => check_plain($theme_title),
        '#default_value' => !empty($settings) && $settings != -1 ? $settings : NULL,
        '#empty_value' => BLOCK_REGION_NONE,
        '#options' => system_region_list($key, REGIONS_VISIBLE),
        '#weight' => ($key == $theme_default ? 9 : 10),
        '#multiple' => 1
      );
    }
  }
  $form['#submit'][] = 'block_order_admin_settings_form_submit';
  return system_settings_form($form);
}

/**
 * Submit handler for settings form().
 */
function block_order_admin_settings_form_submit($form, &$form_state) {
  // Store settings per theme
  foreach ($form_state['values']['regions'] as $theme => $region) {
    variable_set('block_order_' . $theme . '_regions', $region);
  }
}
